document.addEventListener('DOMContentLoaded', function()
{
  var retrouvailles = new Date("Thu Oct 29 2020 19:00:00 UTC+1"),
    /*noel = new Date("Wed Dec 25 2019 8:00:00 UTC+1"),*/
    face = 1,
    jpimg = document.querySelector('#jpimg'),
    frimg = document.querySelector('#frimg'),
    jpdim = jpimg.naturalHeight / jpimg.naturalWidth,
    frdim = frimg.naturalHeight / frimg.naturalWidth,
    to;

  function oui()
  {
    if((new Date()).getTime() < retrouvailles.getTime())
    {
      let time = retrouvailles.getTime() - (new Date()).getTime();
      let sem = Math.floor(time / (1000 * 60 * 60 * 24 * 7));
      time -= sem * 1000 * 60 * 60 * 24 * 7;
      let j = Math.floor(time / (1000 * 60 * 60 * 24));
      time -= j * 1000 * 60 * 60 * 24;
      let h = Math.floor(time / (1000 * 60 * 60));
      time -= h * 1000 * 60 * 60;
      let m = Math.floor(time / (1000 * 60));
      time -= m * 1000 * 60;
      let s = Math.floor(time / (1000));

      $('#oui').text((sem > 0 ? (sem > 1 ? sem + " semaines " : sem + " semaine ") : "") + (j > 0 ? (j > 1 ? j + " jours " : j + " jour ") : "") + (h > 0 ? (h > 1 ? h + " heures " : h + " heure ") : "") + (m > 0 ? (m > 1 ? m + " minutes " : m + " minute ") : "") + "et " + (s > 1 ? s + " secondes" : s + " seconde"));

      /*let a = new Date();
      if(a.getDate() == 26 || (a.getDate() == 25 && a.getUTCHours() >= 15) || (a.getDate() == 27 && a.getUTCHours() < 2))
      {
        $('#scene').css('display', 'block');
      }*/

      clearTimeout(to);
      to = setTimeout(function()
      {
        oui();
      }, 200);
    }
    else
    {
      $('#oui').innerText = "OUI"
    }

    /*if((new Date()).getTime() < noel.getTime())
    {
      let time = noel.getTime() - (new Date()).getTime();
      let sem = Math.floor(time / (1000 * 60 * 60 * 24 * 7));
      time -= sem * 1000 * 60 * 60 * 24 * 7;
      let j = Math.floor(time / (1000 * 60 * 60 * 24));
      time -= j * 1000 * 60 * 60 * 24;
      let h = Math.floor(time / (1000 * 60 * 60));
      time -= h * 1000 * 60 * 60;
      let m = Math.floor(time / (1000 * 60));
      time -= m * 1000 * 60;
      let s = Math.floor(time / (1000));

      $('#oui2').text((sem > 0 ? (sem > 1 ? sem + " semaines " : sem + " semaine ") : "") + (j > 0 ? (j > 1 ? j + " jours " : j + " jour ") : "") + (h > 0 ? (h > 1 ? h + " heures " : h + " heure ") : "") + (m > 0 ? (m > 1 ? m + " minutes " : m + " minute ") : "") + "et " + (s > 1 ? s + " secondes" : s + " seconde"));

      clearTimeout(to);
      to = setTimeout(function()
      {
        oui();
      }, 200);
    }
    else
    {
      $('#oui2').innerText = "OUI"
    }*/
  }

  function heures()
  {
    let h = (new Date).getUTCHours();
    let m = (new Date).getUTCMinutes();
    let s = (new Date).getUTCSeconds();

    $('#jph').text((((h + 9) % 24) < 10 ? '0' : '' ) + ((h + 9) % 24) + ' : ' + (m < 10 ? '0' : '' ) + m + ' : ' + (s < 10 ? '0' : '' ) + s );
    $('#frh').text((((h + 2) % 24) < 10 ? '0' : '' ) + ((h + 2) % 24) + ' : ' + (m < 10 ? '0' : '' ) + m + ' : ' + (s < 10 ? '0' : '' ) + s );
    setTimeout(function()
    {
      heures();
    }, 200);
  }

  function de(max)
  {
    return (Math.floor(Math.random() * (1000000) % max) + 1)
  }

  function rotation(side)
	{
    face = side;
		switch (side)
		{
			case 1:
				$('#d100').css({'transform': 'rotate3d(1,0,0,-90deg)',
                      '-webkit-transform': 'rotate3d(1,0,0,-90deg)',
                      '-moz-transform': 'rotate3d(1,0,0,-90deg)',
                      '-o-transform': 'rotate3d(1,0,0,-90deg)'});
				break;

			case 2:
				$('#d100').css({'transform': 'rotate3d(0,1,0,180deg)',
                      '-webkit-transform': 'rotate3d(0,1,0,180deg)',
                      '-moz-transform': 'rotate3d(0,1,0,180deg)',
                      '-o-transform': 'rotate3d(0,1,0,180deg)'});
				break;

			case 3:
				$('#d100').css({'transform': 'rotate3d(0,1,0,90deg)',
                      '-webkit-transform': 'rotate3d(0,1,0,90deg)',
                      '-moz-transform': 'rotate3d(0,1,0,90deg)',
                      '-o-transform': 'rotate3d(0,1,0,90deg)'});
				break;

			case 4:
				$('#d100').css({'transform': 'rotate3d(0,1,0,0deg)',
                      '-webkit-transform': 'rotate3d(0,1,0,0deg)',
                      '-moz-transform': 'rotate3d(0,1,0,0deg)',
                      '-o-transform': 'rotate3d(0,1,0,0deg)'});
				break;

			case 5:
				$('#d100').css({'transform': 'rotate3d(0,1,0,-90deg)',
                      '-webkit-transform': 'rotate3d(0,1,0,-90deg)',
                      '-moz-transform': 'rotate3d(0,1,0,-90deg)',
                      '-o-transform': 'rotate3d(0,1,0,-90deg)'});
				break;

			case 6:
				$('#d100').css({'transform': 'rotate3d(1,0,0,90deg)',
                      '-webkit-transform': 'rotate3d(1,0,0,90deg)',
                      '-moz-transform': 'rotate3d(1,0,0,90deg)',
                      '-o-transform': 'rotate3d(1,0,0,90deg)'});
				break;
		}
	}

  function redimension()
  {
    pdim = jpimg.naturalHeight / jpimg.naturalWidth;
    frdim = frimg.naturalHeight / frimg.naturalWidth;
    let jp = 0.4 * $('#heures').innerWidth() * jpdim;
    let fr = 0.4 * $('#heures').innerWidth() * frdim;
    $('#heures').innerHeight(Math.min(jp,fr, 0.13 * $(window).innerHeight()));
  }

  $('#lancer').click(function()
  {
    for(i = 1; i < 7; i++)
    {
      if(i != face)
      {
        $('#face' + i).text(de(100));
      }
    }
    do
    {
      var d6 = de(6);
      console.log(d6);
    }
    while (d6 == face);
    rotation(d6);
  });
  $('#jp').click(function()
  {
    $('#prog').css('display', 'flex');
  });
  $('#prog').dblclick(function()
  {
    $('#prog').css('display', 'none');
  });
  $(document).keydown(function(e)
  {
    if(e.key === 'Escape')
    {
      $('#prog').css('display', 'none');
    }
    else if(e.key === ' ')
    {
      for(i = 1; i < 7; i++)
      {
        if(i != face)
        {
          $('#face' + i).text(de(100));
        }
      }
      do
      {
        var d6 = de(6);
        console.log(d6);
      }
      while (d6 == face);
      rotation(d6);
    }
  });
  $(window).resize(function() {
		redimension();
	});

  oui();
  heures();
});
